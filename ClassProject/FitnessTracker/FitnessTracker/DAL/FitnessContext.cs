namespace FitnessTracker.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class FitnessContext : DbContext
    {
        public FitnessContext()
            : base("name=FitnessContext")
        {
        }

        public virtual DbSet<Athlete> Athletes { get; set; }
        public virtual DbSet<Coach> Coaches { get; set; }
        public virtual DbSet<RunData> RunDatas { get; set; }
        public virtual DbSet<Team> Teams { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Coach>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<Coach>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<Coach>()
                .HasMany(e => e.Teams)
                .WithRequired(e => e.Coach)
                .HasForeignKey(e => e.HeadCoach);

            modelBuilder.Entity<RunData>()
                .Property(e => e.Distance)
                .HasPrecision(18, 0);

            modelBuilder.Entity<RunData>()
                .Property(e => e.RunTime)
                .HasPrecision(18, 0);

            modelBuilder.Entity<RunData>()
                .Property(e => e.HeartRate)
                .HasPrecision(18, 0);

            modelBuilder.Entity<RunData>()
                .Property(e => e.StepsPMin)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Team>()
                .HasMany(e => e.Athletes)
                .WithOptional(e => e.Team)
                .HasForeignKey(e => e.Squad)
                .WillCascadeOnDelete();
        }
    }
}
