﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FitnessTracker.Controllers
{
    public class UsersController : Controller
    {
        // GET: Users
        public ActionResult Coach()
        {
            return View();
        }

        [HttpPost]
        public ActionResult FileUpload(IEnumerable<HttpPostedFileBase> selectedFiles) {
           /* foreach (var file in selectedFiles)
            {
                Save to db code here...
            }
            */
            return Json("file uploaded successfully");
        }
    }
}