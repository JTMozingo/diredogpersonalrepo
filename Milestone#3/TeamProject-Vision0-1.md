## Vision Statement 0.1  
MotoHub is a central application for motor sports clubs organizers to manage events, members, and committee easily. Through MotoHub, organizers can set up events, provide registration documents and online forms, maintain a schedule, and view stats on Events and club members. Club members can easily apply and look at a Motor Clubs information and event schedule and register online. 

## Vision Statement 0.9
Motohub is the apex of the integration of social circles and event management and planning. Motor sports club organizers can manage events, members and committees easily. Through MotoHub, club owners and event hosts can organize and distribute their registration documents and online forms, maintain their schedules, create budget plans and view up-to-date analytics and stats on their events and club members.
We provide a user centric space for all Club Members to interact with each other. They can sign-up for events and apply for club membership, view detailed information on clubs and events. MotoHub also offers a space for Club Members to show off their vehicles and connect with other enthusiasts within the MotoHub network.
