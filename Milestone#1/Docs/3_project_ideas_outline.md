---
title: Project Ideas    
layout: default
---

## Project Ideas

A short ouline of 3 project ideas for the team of Alex LeClerc, JT Mozingo, and TJ Dennis for the CS 461 Spring term Software Engineering Class.
The basic outline of each project is that it has to be implemented using C# development on Visual Studios IDE and implemented using the Azure web services. In side of each project there must be some implementation of an API from a known source and that the project be large enough in scope that it cannot be finished in mere days. 

### Idea 1: Party App
For the first idea we considered creating an application that will allow a group of people to share and add to a music playlist so that only 1 device has to stream the audio to a hardware device. A typical use case would be to have the main user to have a spotify (or similar) account, then those who were invited into the group can make suggestions on what to play next and to build up a queue without having to switch devices. This would allow for anyone to choose the music.
    
### Idea 2: Security System
With the ease of access with the cloud and with AI devices, we thought of integrating a home based system with our phones so that we can control and monitor our homes remotely. Since the infrastructure of Alexa, Google, and other systems that allow you to extend their software the consideration is that you dont have to buy into a security package that costs a recurring bill each month and instead monitor everything on your own terms. 
    
### Idea 3: Event Organizer
Most events dont have a platform that can be tailored to their needs and desires. Larger events such as concerts and graduations have to be largely self organized and making sure that everyone is on the same page at the same time is nearly impossible. An application that is designed to work with the needs of an organizer such that everyone involved can quickly provide feedback, information, and RSVP would help. 