# Needs / Features

* Uses NHTSA API
    * Users can use the NHTSA data set to add cars to their garage
    * Can be used to easily search for make & model of car for easier registration
    * List of parts and modifications can be added to the garage profile
* User registration
    * Each user has a QR code for easy registration
        * This can be displayed on a badge or membership card
        * A user is granted one unique QR code
            * The QR code holds the information for the USER and their registration details so event organizers can quickly scan and get a confirmation of registration status.
    * User can register cars in a 'garage'
        * A garage can have multiple cars associated with a user
            * The information in the garage can be used as a social aspect, connecting users with similar garages/cars 
        * Pull information from NHTSA API to determine parts on users car and make notes on modifications
    * Register for events either by: 
        * downloading registration materials and reuploading them to the site 
        * using an online form and submitting it to the site

* Club Committee management tools
    * Certain users are part of club committees
    * Host events with CRUD priveleges
    * Upload event and club materials 
        * Central area to hold forms for all club admins (DropBox API, box, etc)
    * Mass emailing for event updates/news
    * Admin dashboard/analysis of events
    * Database view of club members and payment status
    * Area for budget management as it relates to club member fees stored in the database
* Event information pages 
    * Scheduling calendars and information 
    * event materials such as registration forms, liability, event details, maps, etc
    
* The parent site can be used as a "hub" to search for local and remote clubs and events so that users can attend multiple clubs events

    

