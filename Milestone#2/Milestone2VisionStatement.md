---
title: Vision Statement
layout: default
---

## Vision Statement for Class Project

Here is the outline of the basic requirements and the general needs of the application created around the class project.

### Coach and Athlete workout training tracker. 

For atheletes and coaches who need to monitor their progress as they train with workouts of varying intensity the Fitness Tracker anaylizes data from garmin gps watches, heart rate monitors and foot pods as a web application. Fitness Tracker provides individual and team based analysis by monitoring trends in workouts using integrated garmin technologies to provide in depth training review data. As an assistant to a team's coach Fitness Tracker allows for tailoring of work outs on an individual basis of each athlete unlike many of our competitors that only provides race day data. 