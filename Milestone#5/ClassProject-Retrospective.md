# Diredog
## Milestone 5: Class Project
### Project retrospective

**Safety Check**  
Everyone is at a 4/5, open to communicate, no obstacles in brining up subjects.
___
**What did you learn from the sprint?**  
1. The database and any changes need to be introduced early.
2. Turning in all pull requests at a deadline before the end of the sprint is most effective 
___
**What still puzzles you?**  
The team is still not as compfortable with the distributed workflow as we would like to be, and we all are tenative to make changes. 

___
**What can the team do better during the next Sprint?**  
1. We need to ensure that the database, any changes to the database, and models are established early in the sprint to ensure consistency and reduce the potential for merge conflicts. 
2. All pull requests must religiously be pulled into the repo on Saturday night.
3. We would like to set up a system for Git and for the sprints that gives us more security and ease for solving merge conflicts.